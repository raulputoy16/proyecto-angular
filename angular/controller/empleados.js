 app.controller("EmpleadoController" ,['$scope','$localStorage','ApiClass',function($scope,$localStorage,Api){
     
     local = $scope;

     local.empleado={};
     local.empleado.nombre='';
     local.empleado.cedula='';
     local.empleado.correo="";
     local.empleado.telefono='';
     local.empleado.cargo='';
     local.empleado.grupo='';

     local.cargos={};
     local.grupos={};
     local.empleados=[];

    angular.element(document).ready(function () {
      getdata();
    });


     local.limpiar=function(){

		local.empleado.nombre='';
		local.empleado.cedula='';
		local.empleado.correo='';
		local.empleado.telefono='';
		local.empleado.cargo='';
		local.empleado.grupo='';
     };

    local.guardarempleado=function(){
         Api.GuardarEmpleado(local.empleado,function(response){
         	 console.log(response);
       	     local.empleados=response;
         })
    };


    getdata=function(){
    	   GetEmpleados();
    	  getCargos();
    	  getGrupos();
    	 
    }
 
    getCargos= function(){
       Api.getCargos(function(response){
       	  local.cargos=response;
       });
    }

    getGrupos= function(){
       Api.getGrupos(function(response){
       	  local.grupos=response;
       });
    }

    GetEmpleados= function(){
    	 Api.GetEmpleados(function(response){
       	  local.empleados=response;
          console.log(local.empleados);
       });
    }
    
    local.GetEmpleadoEditar=function(e){
       local.empleado= e;
    }
    
    local.EliminarEmpleado=function(e){
    	alertify.confirm("Esta Seguro de eliminar el registro?", function (element) {
		    if (element) {
		       
                  Api.EliminarEmpleados(e,function(response){
		       	      alertify.success("el registro se elimino exitosamente");
		       	     local.empleados=response;
                  });

		    } else {
		        alertify.success("el registro se mantiene");
		    }
		});
    }


 }]);
