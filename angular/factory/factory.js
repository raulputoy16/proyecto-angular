app.factory("ApiClass",['$localStorage',function($localStorage){

   var fac= {};

   fac.getCargos= function(callback){
         var cargos = $localStorage.cargos;
         callback(cargos);
   };

   fac.getGrupos= function(callback){
         var grupos = $localStorage.grupos;
         callback(grupos);
   };

   fac.GuardarEmpleado= function(empleado,callback){
        var empleados =  $localStorage.empleados;
        var pos = 0;
        var empleadopos=empleado;

        if(empleados==null){
            empleados=[];
            pos=1;
        }
        else{
        	pos= empleados.length+1;
        }
       empleadopos.id= pos;
       empleados.push(empleadopos);
       $localStorage.empleados= empleados;
       callback(empleados);

   };

   fac.GetEmpleados= function(callback){
     var empleadosobj = $localStorage.empleados;
     var empleados =[]
    if(empleadosobj!= undefined ){
    	empleadosobj.forEach(function(element, index, array){
     	empleados= array;
     });
    }
     callback(empleados);
   }
   
   fac.EliminarEmpleados =function(empleado,callback){
     var empleados =  $localStorage.empleados;
     var empleadosnew=[];
     empleados.forEach(function(element, index, array){
     	  if(element.id!=empleado.id){
             empleadosnew.push(element);
     	  }
     });

      $localStorage.empleados= empleadosnew;
      callback(empleadosnew);

   }

   return fac;

}]);