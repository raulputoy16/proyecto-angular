 var app= angular.module("app",["ngStorage"]);

app.run(["$localStorage",function($localStorage){

   var cargos={ "cargos": [
      { "id":1,"nombre":"Admistrador"},
      { "id":2,"nombre":"Programador"},
      { "id":3,"nombre":"Analista"},
      { "id":4,"nombre":"Tester"},
      { "id":5,"nombre":"Supervisor"}]
   }
    var grupos={ "grupos": [
      { "id":1,"nombre":"Star Team"},
      { "id":2,"nombre":"Dedalus Team"},
      { "id":3,"nombre":"Movil Team"},
      { "id":4,"nombre":"Call Center"},
      { "id":5,"nombre":"Administracion"}]
   }

   $localStorage.cargos=   cargos;
   $localStorage.grupos=   grupos;
}]);
